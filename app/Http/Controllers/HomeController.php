<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpWord;
use Dompdf\Dompdf;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \View::addExtension('html', 'php');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $data = [];
      $phpWord = \PhpOffice\PhpWord\IOFactory::load('docs/ddc.docx');
      $htmlWriter = new \PhpOffice\PhpWord\Writer\HTML($phpWord);
      $htmlWriter->save('docs/ddcontr.html');
      $d = new \DOMDocument;
      $mock = new \DOMDocument;
      $d->loadHTML(file_get_contents('docs/ddcontr.html'));
      $body = $d->getElementsByTagName('body')->item(0);
      foreach ($body->childNodes as $child){
          $mock->appendChild($mock->importNode($child, true));
      }
      $data['ddcontr'] = $mock->saveHTML();
      return view('home', $data);
    }

    /**
     * Save the actual contract.
     *
     * @param request
     *
     * @return json
     */
    public function saveddc(Request $request)
    {
      $data = [];
      $ddc = $request->request->get('data');
      file_put_contents('docs/tmp.html', $ddc);
      $data['success'] = true;
      return json_encode($data);
    }

    /**
    **
    **  Display the PDF
    **
    ** @return Response
    **/
    public function getdoc()
    {
      $data = [];
      $ddc = file_get_contents('docs/tmp.html');
      $dompdf = new Dompdf();
      $dompdf->loadHtml($ddc);
      $dompdf->setPaper('A4', 'landscape');
      $dompdf->render();
      $dompdf->stream('contract.pdf',["Attachment"=>1, 'compress'=>0]);
    }
}
