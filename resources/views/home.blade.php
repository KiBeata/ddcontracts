@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                  <input type="button" class="btn btn-info" value="Generate contract" id="getContract"/>
                  <div class="contractspace">
                    <span>
                      {!!  $ddcontr !!}
                    </span>
                    <div class="endnote">
                        <input type="button" class="btn btn-success" value="Generate final PDF" id="getPDFContract"/>
                        <a href="/getdoc" id="displayPDF" class="hiddenLink">Download</a>
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
