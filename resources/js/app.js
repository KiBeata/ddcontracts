/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#getContract').click(function(e){
  e.preventDefault();
  var dsp = $('.contractspace').css('display');
  if(dsp == 'none') {
    $('.contractspace img').css('width','160px');
    $('.contractspace img').css('height', '86px');
    var keyEdits = ['NAME', 'COMPANY NAME', 'ADDRESS', 'COUNTRY', 'NEV',
                    'PROJECT NAME', 'XXYZ', 'listed project for:a+',
                    'XXX', 'XXX.COM', '____:e+'];
    $('.contractspace').css('display','flex');
    for(var i=0; i<keyEdits.length;++i) {
      if($("#gin-"+i).val()===undefined) {
        var placeModifier = keyEdits[i].substr(-3);
        switch (placeModifier) {
          case ":a+":
            keyEdits[i] = keyEdits[i].substring(0, keyEdits[i].length-3);
            realtext = $("p span:contains('"+keyEdits[i]+"')")
                          .next().html();
            if(realtext !== undefined){
              var hc = "<input type='text' class='hiddenInput' ";
              hc += " value='"+realtext+"' style='width:4rem;'";
              hc += " id='gin-"+i+"'/>";
              $("p span:contains('"+keyEdits[i]+"')").first().next().html(hc);
            }
            break;
          case ":e+":
            keyEdits[i] = keyEdits[i].substring(0, keyEdits[i].length-3);
            $("p span:contains('"+keyEdits[i]+"')").filter(function(j){
                var hc = "<input type='text' class='hiddenInput' ";
                hc += " value='"+$(this).html()+"' style='width:15rem;'";
                hc += " id='gin-"+i+"'/>";
                $(this).html(hc);
            });
            break;
          default:
            var content = $("p span:contains('"+keyEdits[i]+"')").first().html();
            if(content!==undefined && !content.includes("hiddenInput")
                && (content.match(/(\<[a-z]+)/g)==null)) {
              var hc = "<input type='text' class='hiddenInput' ";
              if(i==8) {
                var length = 14;
              } else {
                length = content.length;
              }
              hc += " value='"+content+"' style='width:"+length+"rem;'";
              hc += " id='gin-"+i+"'/>";
              $("p span:contains("+keyEdits[i]+")").first().html(hc);
            }
            break;
        }
 }
    }
    $('.hiddenInput').first().addClass('redHidden');
    $(".contractspace p").filter(function(j){
      if(j>22) {
        if($(this).html().trim()!=='') {
          $(this).append('<input type="button" class="btn btn-info btn-sm" value="+"/>');
        }
      }
    })
    $('html, body').animate({
        scrollTop: $(".redHidden").offset().top
    }, 2000);
    $(".hiddenInput").addClass('redHidden');
  }
});

$('.contractspace span').dblclick(function(){
  var content = $(this).html();
  if(!content.includes("input type")) {
    $(this).html("<input type='text' value='"+content+"' class='editInput'/>").focus();
  }
});
$(document).on('focusout', '.editInput', null, function(){
  console.log('blur');
  var content = $(this).val();
  console.log(content);
  $(this).parent().html(content);
});

$(document).on('click', '.contractspace .btn-info', null, function(){
  var elem = $(this).parent();
  var newarea = "<textarea class='insertableArea' placeholder='insert text here' value=''/>";
  elem.after(newarea);
});

$(document).on('focusout', '.insertableArea', null, function(){
  var content = $(this).val();
  $(this).prev().after('<p><span>'+content+'</span></p><p><input type="button" value="+" class="btn btn-info"></p>');
  $(this).remove();
});

$("#getPDFContract").click(function(e){
  $(".redHidden").filter(function(i){
    var content = $(this).val();
    $(this).parent().html(content);
  });
  $('.btn-info').remove();
  $('textarea').remove();
  var contract = $('.contractspace span:first-child').html();
  $.post('/saveddc', {"data":contract, "_token": $('meta[name=csrf-token]').attr('content')}, function(res, stat, obj){
    if(stat=='success') {
      console.log('clicking');
      $("#displayPDF").addClass('btn btn-success');
      $("#displayPDF").removeClass("hiddenLink");
    } else {
      alert("Error "+stat+"!");
    }
  }, 'json');
});
